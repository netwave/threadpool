// TPTest.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include "ThreadPool.h"

using namespace std;

void caca()
{
	cout << "caca" << endl;
}
int main(int argc, char* argv[])
{
	//Locked test
	//ThreadPool tp1( ThreadPool::LOCKED );
	//auto f = [](int waitnum, int tid)
	//{
	//	for (int i = 0; i < waitnum; i++)
	//	{
	//		cout << "Thread "<< tid << " wait "<< i << endl;
	//	}
	//};
	//thread* t1 = tp1.getNewThread();
	//*t1 = thread(f, 5, 1);
	//for (int i = 0; i < 5; i++){cout << "Main thread " << i << endl;}
	//t1->join();
	//thread* t2= tp1.getNewThread();
	//*t2 = thread(f, 5, 2);
	//cout << t2->joinable();
	//thread* t3 = tp1.getNewThread();
	//*t3 = thread(f, 5, 3);
	//for (int i = 0; i < 5; i++){cout << "Main thread " << i << endl;}

	//ThreadPool tp2(ThreadPool::OPTIMUM);
	////ThreadPool tp2(ThreadPool::FIXED, 5);
	//cout << tp2.getMaxThreads() << endl;
	//auto f = [](unsigned int waitTime, int tid)
	//{
	//	this_thread::sleep_for(chrono::milliseconds(1000*waitTime));
	//	cout << tid << " ---> Wait for :" << waitTime << endl; 
	//};
	//vector<thread*> v;
	//for (int i = 16; i > 0; i--)
	//{
	//	thread* t = tp2.getNewThread();
	//	*t = thread(f, i, i);
	//	v.push_back(t);
	//}
	//for (auto& t: v){if(t->joinable())t->join();}
	ThreadPool tp2(ThreadPool::OPTIMUM);
	//thread* t = tp2.getNewThread();
	//*t = thread([](){cout << "hello"<< endl;});
	//t->join();
	//*t = thread([](){cout << "hello again"<< endl;});
	//for (int i = 0; i < 100; i ++) cout << i << endl;
	//cout << t->joinable() << endl;
	vector<thread*> vt;
	for (int i = 0; i < 100; i++)
	{
		auto f = [i]()
		{
			auto acum = 0;
			for(int j = 0; j < 10000; j++)
				for (int k = 0; k < 10000; k++)
			{
				acum += j+k;
			}
			cout << i+acum << endl;
		};
		thread* t = tp2.getNewThread(f);
		vt.push_back(t);
		cout << tp2.getActiveThreads() << endl;
	}
	for (auto& tt : vt) if (tt != nullptr &&tt->joinable()) tt->join();
	tp2.joinAll();
	getchar();
	return 0;
}

