#ifndef	_THREAD_POOL_
#define _THREAD_POOL_

#include <vector>
#include <thread>
#include <atomic>
#include <mutex>
using namespace std;

#pragma warning( push )
#pragma warning( disable : 4715 4267)
class ThreadPool
{

public:
	enum Mode
	{
		LOCKED,		//Just 1 thread
		FIXED,		//Custom defined max threads
		OPTIMUM,	//Optimum number of threads, 1 peer core
		UNLOCKED	//Infinite number of threads
	};

private:

	vector<thread*>		m_pool;
	vector<thread*>		m_safeDestroy;
	vector<unsigned int>m_freeThreads;
	mutex				m_freeLock;
	Mode				m_mode;
	unsigned int		m_maxThreads;

	bool				freeSlots();
	void				savePush(int id);
	unsigned int		savePop();

public:

	ThreadPool(Mode mode = OPTIMUM, unsigned int numberOfThreads = 1);
	~ThreadPool();
	template <class Fn, class... Args>
	thread* getNewThread(Fn&& fn, Args&&... args);
	//thread* getNewThread();
	int				getMaxThreads	();
	unsigned int	getTotalThreads	();
	int				getActiveThreads();
	void			joinAll			();
};
#pragma warning( pop )
#endif