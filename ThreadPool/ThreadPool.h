#ifndef	_THREAD_POOL_
#define _THREAD_POOL_

#include <vector>
#include <thread>
#include <atomic>
#include <mutex>
using namespace std;

#pragma warning( push )
#pragma warning( disable : 4715 4267)
class ThreadPool
{

public:
	enum Mode
	{
		LOCKED,		//Just 1 thread
		FIXED,		//Custom defined max threads
		OPTIMUM,	//Optimum number of threads, 1 peer core
		UNLOCKED	//Infinite number of threads
	};

private:
	atomic<unsigned int>m_active;
	vector<thread*>		m_pool;
	vector<thread*>		m_safeDestroy;
	vector<unsigned int>m_freeThreads;
	mutex				m_freeLock;
	Mode				m_mode;
	unsigned int		m_maxThreads;

	void				free_threads();
	bool				freeSlots();
	void				savePush(int id);
	unsigned int		savePop();

public:
	ThreadPool(Mode mode = OPTIMUM, unsigned int numberOfThreads = 1);
	~ThreadPool();
	template <class Fn, class... Args>
	thread* getNewThread(Fn&& fn, Args&&... args)
	{
		auto callback = [&, this](unsigned int id)
		{
			fn(args...);
			savePush(id);
		};
		m_active.fetch_add(1);
		switch (m_mode)
		{
		case ThreadPool::UNLOCKED:
		{
			if (freeSlots())
			{
				unsigned int id = savePop();
				m_safeDestroy.push_back(m_pool[id]);
				m_pool[id] = new thread(callback, id);
				return m_pool[id];
			}
			thread* newT = new thread(callback, m_pool.size());
			m_pool.push_back(newT);
			return newT;
		}
			break;
		default:
			if (m_pool.size() < m_maxThreads)
			{
				//if (freeSlots())
				//{
				//	unsigned int id = savePop();
				//	m_safeDestroy.push_back(m_pool[id]);
				//	m_pool[id] = new thread(callback, id);
				//	return m_pool[id];
				//}
				thread* newT = new thread(callback, m_pool.size());
				m_pool.push_back(newT);
				return newT;
			}
			else if (m_pool.size() == 1 && m_maxThreads == 1)
			{
				while (!freeSlots()) this_thread::yield();
				m_safeDestroy.push_back(m_pool[0]);
				thread* newT = new thread(callback, 0);
				m_pool[0] = newT;
				return m_pool[0];
			}
			else
			{
				while (!freeSlots()) this_thread::yield();
				unsigned int id = savePop();
				m_safeDestroy.push_back(m_pool[id]);
				m_pool[id] = new thread(callback, id);
				return m_pool[id];
			}
			break;
		}
	}
	//thread* getNewThread();
	int				getMaxThreads	();
	unsigned int	getTotalThreads	();
	unsigned int	getActiveThreads();
	void			joinAll			();
};
#pragma warning( pop )
#endif