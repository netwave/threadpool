#include "ThreadPool.h"



ThreadPool::ThreadPool( Mode mode /*= OPTIMUM*/, unsigned int numberOfThreads /*= 1*/ )
{
	m_mode = mode;
	int numberOfCores = thread::hardware_concurrency();
	switch (mode)
	{
	case ThreadPool::LOCKED:
		m_maxThreads = 1;
		break;
	case ThreadPool::FIXED:
		m_maxThreads = numberOfThreads;
		break;
	case ThreadPool::OPTIMUM:
		if (numberOfCores > 0)	m_maxThreads = numberOfCores;
		else					m_maxThreads = 1;
		break;
	case ThreadPool::UNLOCKED:
		m_maxThreads = 0;
		break;
	default:
		break;
	}
}

ThreadPool::~ThreadPool()
{
	joinAll();
	free_threads();

}

void ThreadPool::free_threads()
{
	for (unsigned int i = 0; i < m_safeDestroy.size(); i++)
	{
		if (m_safeDestroy[i] != nullptr && m_safeDestroy[i]->joinable())
		{
			m_safeDestroy[i]->join();
			delete m_safeDestroy[i];
		}
	}
	m_safeDestroy.clear();
}

bool ThreadPool::freeSlots()
{
	while (!m_freeLock.try_lock()) this_thread::yield();
	bool ret = m_freeThreads.size() > 0;
	m_freeLock.unlock();
	return ret;
}


void ThreadPool::savePush(int id)
{
	while (!m_freeLock.try_lock()) this_thread::yield();
	m_freeThreads.push_back(id);
	m_freeLock.unlock();
	m_active.fetch_sub(1);
}

unsigned int ThreadPool::savePop()
{
	while (!m_freeLock.try_lock()) this_thread::yield();
	unsigned int ret = m_freeThreads[m_freeThreads.size() - 1];
	m_freeThreads.pop_back();
	m_freeLock.unlock();
	return ret;
}

//template <class Fn, class... Args>
//thread* ThreadPool::getNewThread(Fn&& fn, Args&&... args)
//{
//	switch (m_mode)
//	{
//	case ThreadPool::UNLOCKED:
//	{
//		for (unsigned int i = 0; i < m_pool.size(); i++)
//		{
//			if (m_pool[i] == nullptr || !m_pool[i]->joinable())
//			{
//				delete m_pool[i];
//				m_pool[i] = new thread(fn, args...);
//				return m_pool[i];
//			}
//		}
//		thread* newT = new thread(fn, args...);
//		m_pool.push_back(newT);
//		return newT;
//	}
//		break;
//	default:
//		if (m_pool.size() < m_maxThreads)
//		{
//			for (unsigned int i = 0; i < m_pool.size(); i++)
//			{
//				if (m_pool[i] == nullptr || !m_pool[i]->joinable())
//				{
//					delete m_pool[i];
//					m_pool[i] = new thread(fn, args...);
//					return m_pool[i];
//				}
//			}
//			thread* newT = new thread(fn, args...);
//			m_pool.push_back(newT);
//			return newT;
//		}
//		else if (m_pool.size() == 1 && m_maxThreads == 1)
//		{
//			if (m_pool[0]->joinable()){ m_pool[0]->join(); }
//			delete m_pool[0];
//			thread* newT = new thread(fn, args...);
//			m_pool[0] = newT;
//			return m_pool[0];
//		}
//		else
//		{
//			for (unsigned int i = 0; i < m_pool.size(); i++)
//			{
//				if (m_pool[i] == nullptr || !m_pool[i]->joinable())
//				{
//					//thread* newT = new thread();
//					//m_pool.push_back(newT);
//					delete m_pool[i];
//					m_pool[i] = new thread(fn, args...);
//					return m_pool[i];
//				}
//			}
//			for (unsigned int i = 0; i < m_pool.size(); i++)
//			{
//				if (m_pool[i] != nullptr && m_pool[i]->joinable())
//				{
//					m_pool[i]->join();
//					delete m_pool[i];
//					m_pool[i] = new thread(fn, args...);
//					return m_pool[i];
//				}
//			}
//		}
//		break;
//	}
//}

int ThreadPool::getMaxThreads()
{
	if(m_mode == UNLOCKED) return -1;
	return m_maxThreads;
}


unsigned int ThreadPool::getTotalThreads()
{
	return (unsigned int)m_pool.size();
}

unsigned int ThreadPool::getActiveThreads()
{
	//int count = 0;
	//for (auto& t : m_pool) if(t != nullptr && t->joinable()) count++;
	//return count;
	return m_active.load();
}

void ThreadPool::joinAll()
{
	for (unsigned int i = 0; i < m_pool.size(); i++)
	{
		if (m_pool[i] != nullptr && m_pool[i]->joinable())
		{
			m_pool[i]->join();
			m_safeDestroy.push_back(m_pool[i]);
		}
	}
	m_pool.clear();
	free_threads();
	m_freeThreads.clear();
}